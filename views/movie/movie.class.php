<?php

include_once('../../semsol/ARC2.php'); /* ARC2 static class inclusion */ 

include_once('../../files/general.func.php');

class movie {
	
	/* Set the searchkey and initialize a sparql connection */
	public function __construct($searchkey) {
		$this->searchkey = $searchkey;
		
		$dbpconfig = array(
		  	"remote_store_endpoint" => "http://www.linkedmdb.org/sparql",
		 );
 
  		$this->store = ARC2::getRemoteStore($dbpconfig); 
		
		$this->getData();
	}
	
	private $store;
	
	private $searchkey;
	private $title = '';
	private $director = '';
	private $description = '';
	private $writer = '';
	private $poster = '';
	private $genre = '';
	private $actors = '';
	private $runtime = '';
	private $release = '';
	private $locations = '';
	private $similarendpoint = '';
	private $similar = '';
	
	function getTitle() {
		return $this->title;
	}
	
	function getDirector() {
		$directors = '';
		
		foreach ($this->director as $director) {
			if ($directors != '') {
				$directors .= ', ';
			}
			
			$directors .= '<a class="tool" href="views/movie/ajax-tooltip.php?action=person&type=Director&person='. $director .'">'. $director .'</a>';
		}
		return $directors;
	}
	
	function getDescription() {
		return $this->description;
	}
	
	function getWriter() {
		$writers = '';
		
		foreach ($this->writer as $writer) {
			if ($writers != '') {
				$writers .= ', ';
			}
			
			$writers .= '<a class="tool" href="views/movie/ajax-tooltip.php?action=person&type=Writer&person='. $writer .'">'. $writer .'</a>';
		}
		return $writers;
	}
	
	function getPoster() {
		return $this->poster;
	}
	
	function getGenre() {
		$genres = '';
		
		foreach ($this->genre as $genre) {
			if ($genres != '') {
				$genres .= ', ';
			}
			
			$genres .= $genre;
		}
		return $genres;
	}
	
	function getActors() {
		$actors = '';
		
		foreach ($this->actors as $actor) {
			if ($actors != '') {
				$actors .= ', ';
			}
			$actors .= '<a class="tool" href="views/movie/ajax-tooltip.php?action=person&type=Actor&person='. $actor .'">'. $actor .'</a>';
		}
		
		return $actors;
	}
	
	function getRuntime() {
		return $this->runtime . ' minutes';
	}
	
	function getRelease() {
		return $this->release;
	}
	
	function getLocations() {
		$locations = '';
		
		if (!array_key_exists('0', $this->locations) || $this->locations['0'] == 'No information available') {
			return 'No information available';
		} 
		
		foreach ($this->locations as $location) {
			if ($locations != '') {
				$locations .= ', ';
			}
			$locations .= '<a class="tool" href="views/movie/ajax-tooltip.php?action=location&location='. $location .'">'. $location .'</a>';
		}
		
		return $locations;
	}
	
	function getSimilar() {
		$this->getSimilarMovies();
		return $this->similar;
	}
	
	/* Fill all the data */
	private function getData() {
		$query = '
			PREFIX movie: <http://data.linkedmdb.org/resource/movie/>
			PREFIX dc: <http://purl.org/dc/terms/>

			SELECT ?title?resource?genre?date?runtime?release?uri WHERE {
				?resource movie:filmid ?uri .
				?resource dc:title ?title . 
				?resource dc:title "' . $this->searchkey . '" .
				OPTIONAL {
					?resource movie:genre ?genre .
				}
				OPTIONAL {
					?resource dc:date ?date .
				}
				OPTIONAL {
					?resource movie:runtime ?runtime .
				}
				OPTIONAL {
					?resource movie:initial_release_date ?release .
				}
			} LIMIT 1
  		'; 
 
  		$rows = $this->store->query($query, 'rows'); /* execute the query */
		
		$data = $rows['0'];
		
		//Get all actors
		$query_actor = '
			PREFIX movie: <http://data.linkedmdb.org/resource/movie/>
			PREFIX dc: <http://purl.org/dc/terms/>
			
			SELECT ?name WHERE {
				?resource movie:actor ?actor .
				?resource dc:title "' . $this->searchkey . '" .
				
				OPTIONAL {
					?actor movie:actor_name ?name .
				}
			} LIMIT 10
		';
		
		$rows_actor = $this->store->query($query_actor, 'rows');
		
		$query_director = '
			PREFIX movie: <http://data.linkedmdb.org/resource/movie/>
			PREFIX dc: <http://purl.org/dc/terms/>
			
			SELECT ?name WHERE {
				?resource movie:director ?director .
				?resource dc:title "' . $this->searchkey . '" .
				
				OPTIONAL {
					?director movie:director_name ?name .
				}
			} LIMIT 5
		';
		
		$rows_director = $this->store->query($query_director, 'rows');
		
		$query_writer = '
			PREFIX movie: <http://data.linkedmdb.org/resource/movie/>
			PREFIX dc: <http://purl.org/dc/terms/>
			
			SELECT ?name WHERE {
				?resource movie:writer ?writer .
				?resource dc:title "' . $this->searchkey . '" .
				
				OPTIONAL {
					?writer movie:writer_name ?name .
				}
			} LIMIT 5
		';
		
		$rows_writer = $this->store->query($query_writer, 'rows');
		
		$query_genre = '
			PREFIX movie: <http://data.linkedmdb.org/resource/movie/>
			PREFIX dc: <http://purl.org/dc/terms/>
			
			SELECT ?name WHERE {
				?resource movie:genre ?genre .
				?resource dc:title "' . $this->searchkey . '" .
				
				OPTIONAL {
					?genre movie:film_genre_name ?name .
				}
			} LIMIT 5
		';
		
		$rows_genre = $this->store->query($query_genre, 'rows');
		
		$query_location = '
			PREFIX movie: <http://data.linkedmdb.org/resource/movie/>
			PREFIX dc: <http://purl.org/dc/terms/>
			
			SELECT ?name WHERE {
				?resource movie:featured_film_location ?location .
				?resource dc:title "' . $this->searchkey . '" .
				
				OPTIONAL {
					?location movie:film_location_name ?name .
				}
			} LIMIT 5
		';
		
		$rows_location = $this->store->query($query_location, 'rows');
		
		$this->title = ifExists($data['title']);
		
		$this->runtime = ifExists($data['runtime']);
		
		$this->release = ifExists($data['release']);
		
		$this->actors = addToArray($rows_actor);
		
		$this->director = addToArray($rows_director);
		
		$this->writer = addToArray($rows_writer);
		
		$this->genre = addToArray($rows_genre);	
		
		$this->locations = addToArray($rows_location);
		
		$this->getTomatoes();
	}
	
	function getTomatoes() {
		$apikeyRT = "n6nj8hckef68d76nrznk4gmr";
		
		$title = urlencode($this->title);

		$endpoint = 'http://api.rottentomatoes.com/api/public/v1.0/movies.json?apikey='.$apikeyRT.'&q='.$title.'&page_limit=1';

		// setup curl to make a call to the endpoint
		$session = getCurlSession($endpoint);

		// exec curl and get the data back
		$data = curl_exec($session);

		// remember to close the curl session once we are finished retrieveing the data
		curl_close($session);

		// decode the json data to make it easier to parse the php
		$movie = json_decode($data);
		if ($movie === NULL) die('Error parsing json');

		if (property_exists($movie, 'error')) { return false; }
		
		$this->description = $movie->movies[0]->synopsis;
		$this->poster = str_replace('_tmb', '_det', $movie->movies[0]->posters->detailed);
		
		$this->similarendpoint = $movie->movies[0]->links->similar;	
	}
	
	function getSimilarMovies() {
		$apikeyRT = "n6nj8hckef68d76nrznk4gmr";
		$endpoint = $this->similarendpoint;
		
		$session = getCurlSession($endpoint . '?apikey=' . $apikeyRT);
		$data = curl_exec($session);		
		curl_close($session);
		
		$movie = json_decode($data);
		if ($movie === NULL) die('Error parsing json');

		if (property_exists($movie, 'error')) { return false; }

		$i = 0;
		foreach($movie->movies as $key => $value) {
			$this->similar[$i]['title'] = $value->title;
			$this->similar[$i]['poster'] = str_replace('_tmb', '_det', $value->posters->detailed);
			$i++;
		}
		
		
	}
	
}

?>