Tutorial SPARQL PHP:
http://cui.unige.ch/isi/icle-wiki/php_sparql_endpoints

Film opendata:
http://linkedmdb.org/
handige test voor linkedmdb
www.linkedmdb.org/snorql/

More examples:
http://answers.semanticweb.com/questions/8845/example-sparql-queries-on-lod-cloud
http://stackoverflow.com/questions/18783869/linkedmdb-sparql-query
http://stackoverflow.com/questions/18788134/get-movies-actor-acted-in-linkedmdb

Freebase.com
http://www.freebase.com
https://groups.google.com/forum/#!topic/sindicetech-freebase/93PBGJBnnIU

Handige tutorial:
http://arxiv.org/pdf/1408.4793.pdf