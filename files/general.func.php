<?php

/* Checks if var exists, if not return a default value */
function ifExists(&$var, $default = 'Unknown') {
	return isset($var) ? $var : $default;
}

/* Get data from one field and add it to a new array */
function addToArray($input, $fieldname = 'name') {
	$array = array();
	
	if (empty($input)) {
		$array[] = 'No information available';
		
		return $array;
	}
	
	foreach($input AS $row) {
		if (array_key_exists($fieldname, $row)) {
			$array[] = $row[$fieldname];
		}
	}
	
	return $array;
}

/**
 * Get a basic curl session returned
 * @param type $url
 * @return type
 */
function getCurlSession($url) {
    $session = curl_init($url);
    
    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($session, CURLOPT_ENCODING, '');
    
    return $session;
}

function truncate($string,$length=750,$append="&hellip;") {
  $string = trim($string);

  if(strlen($string) > $length) {
    $string = wordwrap($string, $length);
    $string = explode("\n", $string, 2);
    $string = $string[0] . $append;
  }

  return $string;
}

?>