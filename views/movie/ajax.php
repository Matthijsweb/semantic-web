<?php 

require_once 'movie.class.php';
require_once 'person.class.php';

if ($_REQUEST['action'] == 'fill') {
	
	$movie = $_REQUEST['film'];
	
	$movie = str_replace('+', '_', $movie);
	
	$movieinfo = new movie($movie);
	
	$data = Array();

	$data['img']['poster'] = $movieinfo->getPoster();
	
	$data['text']['title'] = $movieinfo->getTitle();
	
	$data['text']['director'] = $movieinfo->getDirector();
	
	$data['text']['writer'] = $movieinfo->getWriter();
	
	$data['text']['description'] = $movieinfo->getDescription();
	
	$data['text']['actors'] = $movieinfo->getActors();
	
	$data['text']['genre'] = $movieinfo->getGenre();
	
	$data['text']['runtime'] = $movieinfo->getRuntime();
	
	$data['text']['release'] = $movieinfo->getRelease();
	
	$data['text']['locations'] = $movieinfo->getLocations();
	
	$similarmovies = $movieinfo->getSimilar();
	
	$i = 0;
	
	if (is_array($similarmovies)) {
		while ($i < 5) {

			if (array_key_exists($i, $similarmovies)) {
				$data['similar']['similar' . $i] = $similarmovies[$i];
			} else {
				break;
			}

			$i++;
		}
	}
		
	echo json_encode($data);

}
?>