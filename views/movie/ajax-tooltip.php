<?php

require_once 'person.class.php';
require_once 'location.class.php';
require_once '../../files/general.func.php';

if ($_REQUEST['action'] == 'person') {
	$person = $_REQUEST['person']; 
	$type = $_REQUEST['type'];
	$personinfo = new person($person);?>

	<h3>
		<?php echo $personinfo->getPerson(); ?> (<?php echo $type; ?>)
	</h3>

	<p>
		<b>Born on:</b> <?php echo array_shift(explode('+', $personinfo->getBirthDate())); ?><br>
		<b>Birth place:</b> <?php echo $personinfo->getCountry(); ?>
	</p>

	<p>
		<?php echo $personinfo->getAbstract(); ?>
	</p>

<?php
} else if ($_REQUEST['action'] == 'location') {
	$location = $_REQUEST['location'];
	
	$locationinfo = new locations($location); ?>
	
	<h3>
		<?php echo $locationinfo->getLocation(); ?>	
	</h3>

	<p>
		<?php echo truncate($locationinfo->getAbstract()); ?>
	</p>

	<p>
		Longitude: <?php echo $locationinfo->getLongitude(); ?><br>
		Latitude: <?php echo $locationinfo->getLatitude(); ?><br>
		
		<img class='map' alt='<?php echo $location; ?>' src='http://maps.google.com/maps/api/staticmap?zoom=11&size=250x250&maptype=terrain&sensor=false&center=<?php echo $location; ?>'><br>
	</p>

	<?php
}

?>
