

Test 1 (filmid, title, genre)

ENDPOINT http://www.linkedmdb.org/sparql

PREFIX movie: <http://data.linkedmdb.org/resource/movie/>
PREFIX dc: <http://purl.org/dc/terms/>

SELECT ?label?resource?genre WHERE {
    ?resource movie:filmid ?uri .
    ?resource dc:title ?label . 
    ?resource dc:title "Titanic" .
    ?resource movie:genre ?genre
}

select ?name ?demonym { 
  dbpedia:Paul_Hogan dbpedia-owl:birthPlace ?country .
  

  ?country a dbpedia-owl:Country ; rdfs:label ?name .
  optional { ?country dbpedia-owl:demonym ?demonym }

  filter langMatches(lang(?name),"en")
  filter langMatches(lang(?demonym),"en")
}

PREFIX dbp: <http://dbpedia.org/resource/>
PREFIX dbp-owl: <http://dbpedia.org/ontology/>

select ?name ?country ?birth { 
  dbp:Paul_Hogan dbp-owl:birthPlace ?country .
  
  dbp:Paul_Hogan dbp-owl:birthDate ?birth .
  

  ?country a dbp-owl:Country ; rdfs:label ?name .

  filter langMatches(lang(?name),"en")
}

PREFIX dbp: <http://dbpedia.org/resource/>
PREFIX dbp-owl: <http://dbpedia.org/ontology/>

select ?name ?birth ?abstract{ 
  dbp:James_Cameron dbp-owl:birthPlace ?country .
  
  dbp:James_Cameron dbp-owl:birthDate ?birth .
     
  dbp:James_Cameron dbp-owl:abstract ?abstract .

  ?country a dbp-owl:Country ; rdfs:label ?name .

  filter langMatches(lang(?name),"en")
  filter langMatches(lang(?abstract),"en")
} LIMIT 1


