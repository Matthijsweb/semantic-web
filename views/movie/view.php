<div class="container" id="computer">
	<rdf:RDF
		xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
		xmlns:actor="http://data.linkedmdb.org/resource/movie/actor"
		xmlns:director="http://data.linkedmdb.org/resource/movie/director"
		xmlns:genre="http://data.linkedmdb.org/resource/movie/genre"
		xmlns:runtime="http://data.linkedmdb.org/resource/movie/runtime"
		xmlns:writer="http://data.linkedmdb.org/resource/movie/writer"
		xmlns:title="http://purl.org/dc/terms/date"
		xmlns:date="http://purl.org/dc/terms/date"><!--- Ik weet niet of dit juist is dit is namelijk een property. net als title. -->
		
		<rdf:Description rdf:about="http://data.linkedmdb.org/resource/film/72"> <!--- In dit geval over de Titanic -->
			<actor:actor_actorid>10361</actor:actor_actorid>
			<actor:actor_name>Lewis Abernathy</actor:actor_name>
		</rdf:Description>

		
	</rdf:Description>
</div>

<!-- Page Content -->
<div class="container" id="human">

	<link href="css/movie.css" rel="stylesheet">
	<script type="text/javascript">
		movietitle = "<?php echo $_REQUEST['movie']; ?>"
	</script>
	<script src="views/movie/handle.js"></script>


	<!-- Portfolio Item Heading -->
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header" id="title">Film
				<small id="director">director</small>
			</h1>
		</div>
	</div>
	<!-- /.row -->

	<!-- Portfolio Item Row -->
	<div class="row">

		<div class="col-md-4">
			<img class="img-responsive" src="" alt="" id="poster">
		</div>

		<div class="col-md-8">
			<h3>Movie Description</h3>
			<p id="description"></p>

			<h3>Film details</h3>

			<table>
				<tr>
					<td>Director:</td>
					<td id="director"></td>
				</tr>
				<tr>
					<td>Writers:</td>
					<td id="writer"></td>
				</tr>
				<tr>
					<td>Actors:</td>
					<td id="actors"></td>
				</tr>
				<tr>
					<td>Genre:</td>
					<td id="genre"></td>
				</tr>
				<tr>
					<td>Runtime:</td>
					<td id="runtime"></td>
				</tr>
				<tr>
					<td>Initial Release:</td>
					<td id="release"></td>
				</tr>
				<tr>
					<td>Film Locations:</td>
					<td id="locations"></td>
				</tr>
			</table>
		</div>

	</div>
	<!-- /.row -->

	<!-- Related Projects Row -->
	<div class="row">

		<div class="col-lg-12">
			<h3 class="page-header">Related Movies</h3>
		</div>

		<div class="col-sm-2 col-xs-6">
			<a href="#">
				<img class="img-responsive portfolio-item" src="" alt="" id="similar0" height="300px" width="200px">
			</a>
		</div>

		<div class="col-sm-2 col-xs-6">
			<a href="#">
				<img class="img-responsive portfolio-item" src="" alt="" id="similar1" height="300px" width="200px">
			</a>
		</div>

		<div class="col-sm-2 col-xs-6">
			<a href="#">
				<img class="img-responsive portfolio-item" src="" alt="" id="similar2" height="300px" width="200px">
			</a>
		</div>

		<div class="col-sm-2 col-xs-6">
			<a href="#">
				<img class="img-responsive portfolio-item" src="" alt="" id="similar3" height="300px" width="200px">
			</a>
		</div>

		<div class="col-sm-2 col-xs-6">
			<a href="#">
				<img class="img-responsive portfolio-item" src="" alt="" id="similar4" height="300px" width="200px">
			</a>
		</div>

	</div>

	<!-- /.row -->

	<hr>