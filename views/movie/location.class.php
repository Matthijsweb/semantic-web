<?php 

include_once('../../semsol/ARC2.php'); /* ARC2 static class inclusion */ 

include_once('../../files/general.func.php');

class locations {
	
	public function __construct($location) {
		$this->location = explode(",",$location)[0];

		$dbpconfig = array(
			"remote_store_endpoint" => "http://dbpedia.org/sparql",
		);

		$this->store = ARC2::getRemoteStore($dbpconfig); 

		$this->getData();
	}
	
	private $store;
	private $location;
	
	private $flagimage;
	private $longitude;
	private $latitude;
	private $description; 
	
	function getLocation() {
		return $this->location;
	}
	
	function getFlagImage() {
		if ($this->flagimage != 'No information available') {
			return  str_replace(' ', '_', $this->flagimage);
		} else {
			return null;
		}
			 
	}
	
	function getLongitude() {
		return $this->longitude;
	}
	
	function getLatitude() {
		return $this->latitude;
	}
	
	function getAbstract() {
		return $this->description;
	}
	
	private function getData() {
		$query = '
			PREFIX geo: <http://www.w3.org/2003/01/geo/wgs84_pos#>
			PREFIX dbo: <http://dbpedia.org/ontology/>
			PREFIX dcterms: <http://purl.org/dc/terms/>
			PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
			PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
			PREFIX dbpprop: <http://dbpedia.org/property/>
					SELECT * WHERE {
					?s a dbo:Place ;
					   rdfs:label ?label .
					OPTIONAL {
						?s geo:lat ?lat .
						?s geo:long ?long .
					}
					OPTIONAL {
						?s geo:lat ?lat .
						?s geo:long ?long .
					}					
					OPTIONAL {
						?s dbpprop:imageFlag ?flagimage .
					}
					OPTIONAL {
						?s dbo:abstract ?abstract .
						FILTER(langMatches(lang(?abstract), "EN"))
					}
					?s dcterms:subject ?sub .
					filter(?label="' . $this->location . '"@en)
			} LIMIT 1
		';
		
		$rows = $this->store->query($query, 'rows');
		
		if( isset($rows['0'])) { 
			$data = $rows['0'];
		}
		
		$this->longitude = ifExists($data['long']);
		$this->latitude = ifExists($data['lat']);
		$this->flagimage = ifExists($data['flagimage']);
		$this->description = ifExists($data['abstract']);
	}
	
}

?>