<?php

include_once('../../semsol/ARC2.php'); /* ARC2 static class inclusion */ 

include_once('../../files/general.func.php');

class person {
	
	/* Set the searchkey and initialize a sparql connection */
	public function __construct($person) {
		$this->person = str_replace(' ', '_', $person);

		$dbpconfig = array(
			"remote_store_endpoint" => "http://dbpedia.org/sparql",
		);

		$this->store = ARC2::getRemoteStore($dbpconfig); 

		$this->getData();
	}
	
	private $store;
	
	private $person;
	private $countryname = 'Unknown';
	private $birthdate = 'Unknown';
	private $description = 'No information could be found.';
	
	function getPerson() {
		return str_replace('_', ' ', $this->person);
	}
	
	function getCountry() {
		return $this->countryname;
	}
	
	function getBirthDate() {
		return $this->birthdate;
	}
	
	function getAbstract() {
		return $this->description;
	}
	
	function getData() {
		$query = '
			PREFIX dbp: <http://dbpedia.org/resource/>
			PREFIX dbp-owl: <http://dbpedia.org/ontology/>

			SELECT ?countryname ?birth ?abstract{ 
				OPTIONAL {
					dbp:' . $this->person . ' dbp-owl:birthPlace ?country .
				}

				OPTIONAL {
					dbp:' . $this->person . ' dbp-owl:birthDate ?birth .
				}

				OPTIONAL {
					dbp:' . $this->person . ' dbp-owl:abstract ?abstract .
					filter langMatches(lang(?abstract),"en")
				}

				OPTIONAL {
					?country a dbp-owl:Country ; rdfs:label ?countryname .
					filter langMatches(lang(?countryname),"en")
				}
			} LIMIT 1
		';
		
		$rows = $this->store->query($query, 'rows'); /* execute the query */
		
		if (array_key_exists('0', $rows)) {
		
			$data = $rows['0'];

			$this->countryname = ifExists($data['countryname']);
			$this->birthdate = ifExists($data['birth']);
			$this->description = ifExists($data['abstract']);
		}
	}
	
}

?>