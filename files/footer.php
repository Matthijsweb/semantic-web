	<footer>
		<div class="row">
			<div class="col-lg-12">
				<p>Copyright &copy; Semantic Web 2014</p>
			</div>
		</div>
		<!-- /.row -->
	</footer>

</div>

<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="files/jquery.qtip.min.js"></script>
	<script type="text/javascript" src="files/imagesloaded.pkg.min.js"></script>
  </body>
</html>