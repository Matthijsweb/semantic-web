$( document ).ready(function() {
	
	loadingScreen();
	
	$("#computer").hide();
	
	fillPage("views/movie/ajax.php");
	
	$('a[title]').qtip({ style: { name: 'cream', tip: true } });
	
	var openicon = $( "#switchmode" );
	var texthuman = "Switch to RDF XML mode";
	var textcomputer = "Switch to HTML mode";
	
	openicon.click(function() {
		if (openicon.attr('mode') == 'computer') {
			$("#computer").hide();
			openicon.attr('mode', 'human');
			openicon.attr('title', textcomputer);
  			$("#human").show();
		} else if (openicon.attr('mode') == 'human') {
			$("#human").hide();
			openicon.attr('mode', 'computer');
			openicon.attr('title', texthuman);
			$("#computer").show();
		}
	});
	
});

var spinner;

function saveFields() {
	saveData("views/movie/ajax.php");
}

//Show loading screen
function loadingScreen() {
	
	setMask(false, 0.8, 0, 0);
	
	spinner = addSpinner('box');
	
/* 	$.mobile.loading( 'show', {
		text: 'Loading page please wait',
		textVisible: true,
		theme: 'a',
		html: ""
	}); */
}

//Get data from ajax and fill page
function fillPage(ajaxurl) {
	var jqxhr = $.ajax({
		dataType: "json",
		url: ajaxurl,
		data: { action: 'fill', film: movietitle}
	})
	.done(function(data) {
		$.each( data.img, function( id, item ) {
			$("#" + id).attr("src", item);
      	});
		
		$.each( data.text, function( id, item ) {
			$("#" + id).html(item);
      	});
		
		if (data.similar) {
			$.each( data.similar, function( id, item ) {
				$("#" + id).attr("src", item['poster']);
				$("#" + id).parent().attr('href', 'movie.php?movie=' + item['title']);
			});
		}
		
		tooltips();
	})
	.fail(function(xhr, ajaxOptions, thrownError) {
		alert("Can't find movie, please try again!");
	})
	.always(function() {
		spinner.stop();
		hideMask();
		//alert( "complete" );
	});
}

function setMask(closable, opacity, fadein, fadeto)
{
	fadein = typeof fadein !== 'undefined' ? fadein : '1000';
	fadeto = typeof fadeto !== 'undefined' ? fadeto : 'slow';
	
    $('.boxes').prepend("<div class=\"mask\"></div>");
    //Get the screen height and width
    var maskHeight = $(document).height();
    var maskWidth = $(window).width() + 25;
    //Set height and width to mask to fill up the whole screen
    $('.mask').css({'width':maskWidth,'height':maskHeight});

    //transition effect
    $('.mask').fadeIn(fadein);
    $('.mask').fadeTo(fadeto, opacity);

    if (closable) {
       $('.mask').click(function () {
           $(this).hide();
           $('.window, .close').hide();
       });

       $(document).keyup(function(e) {
           if(e.keyCode === 27) {
               $('.window, .close, .mask').hide();
           }
       });
    }
}

function hideMask(fadeout) {
	fadeout = typeof fadeout !== 'undefined' ? fadeout : 'slow';
	$('.mask').fadeOut(fadeout);
}

function addSpinner(element) {
	var target = document.getElementById(element);
	var spinner = new Spinner().spin(target);
	return spinner;
}

function tooltips() {
	$('.tool').each(function() {
         $(this).qtip({
            content: {
                text: function(event, api) {
                    $.ajax({
                        url: api.elements.target.attr('href') // Use href attribute as URL
                    })
                    .then(function(content) {
                        // Set the tooltip content upon successful retrieval
                        api.set('content.text', content);
                    }, function(xhr, status, error) {
                        // Upon failure... set the tooltip content to error
                        api.set('content.text', status + ': ' + error);
                    });
        
                    return 'Loading...'; // Set some initial text
                }
            },
            position: {
                viewport: $(window)
            },
            style: 'qtip-wiki'
         });
     });
}
